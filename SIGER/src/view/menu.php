
  
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
        <div class="sidebar-brand-icon rotate-n-15">
        </div>
        <div class="sidebar-brand-text mx-3 fa-2x text-gray-100" <a class="nav-link" href="../view/home.php">SIGER</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="../view/home.php">
          <i class="fas fa-home"></i>
          <span class="fa-2x text-gray-100">Home</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users"></i>
          <span>Usuários</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Custom Components:</h6>-->
            <a class="collapse-item" href="../view/add_usuario.php">Adicionar usuário</a>
            <a class="collapse-item" href="../model/lista_usuarios.php">Lista de usuários</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-tractor"></i>
          <span>Fazendas</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Custom Utilities:</h6>-->
            <a class="collapse-item" href="../model/fazenda_visualizar.php">Visualizar fazenda</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          <i class="fas fa-democrat"></i>
          <span>Animais</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Custom Components:</h6>
            <a class="collapse-item" href="../view/add_usuario.php">Adicionar usuário</a>-->
            <a class="collapse-item" href="../model/lista_animais.php">Lista de animais</a>
            <a class="collapse-item" href="../model/historico_animais.php">Histórico de saídas</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Controle Zootécnico
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-item" aria-expanded="true" aria-controls="collapse-item">
          <i class="fas fa-keyboard"></i>
          <span>Movimentação</span>
        </a>
        <div id="collapse-item" class="collapse" aria-labelledby="heading-item" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Login Screens:</h6>-->
            <a class="collapse-item" href="../view/add_animal.php">Entrada de animais</a>
            <a class="collapse-item" href="../model/lista_animais_saida.php">Saída de animais</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-pills"></i>
          <span>Sanitário</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Login Screens:</h6>-->
            <a class="collapse-item" href="../model/lista_animais_doenca.php">Doenças</a>
            <a class="collapse-item" href="../model/lista_animais_med_pre.php">Medidas Preventivas</a>
            <a class="collapse-item" href="../model/lista_animais_vacinacao.php">Vacinação</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-inner" aria-expanded="true" aria-controls="collapse-inner">
           <i class="fab fa-hubspot"></i>
          <span>Reprodutivo</span>
        </a>
        <div id="collapse-inner" class="collapse" aria-labelledby="heading-inner" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Login Screens:</h6>-->
            <a class="collapse-item" href="../model/lista_animais_cobertura.php">Coberturas</a>
            <a class="collapse-item" href="../model/lista_animais_cof_pre.php">Confirmação de prenhez</a>
            <a class="collapse-item" href="../model/lista_animais_partos.php">Partos</a>
            <a class="collapse-item" href="../model/lista_animais_desmame.php">Desmame</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
           <i class="far fa-paper-plane"></i>
          <span>Produtivo</span>
        </a>
        <div id="collapse" class="collapse" aria-labelledby="heading" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Login Screens:</h6>-->
            <a class="collapse-item" href="../model/lista_animais_pesagem.php">Pesagens</a>
            <a class="collapse-item" href="../model/lista_animais_contro_lei.php">Controle leiteiro</a>
            <a class="collapse-item" href="../model/lista_animais_enc_lactacao.php">Encerramento de lactação</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
           <i class="far fa-chart-bar"></i>
          <span>Relatórios Zootécnicos</span>
        </a>
        <div id="collapsefour" class="collapse" aria-labelledby="heading" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!--<h6 class="collapse-header">Login Screens:</h6>-->
            <a class="collapse-item" href="../model/lista_animais_relatorios.php">Ficha individual</a>
            <a class="collapse-item" href="../model/lista_animais_relatorio_reprodutivo.php">Ficha reprodutiva</a>
            <a class="collapse-item" href="../model/lista_animais_completa.php">Ficha completa</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts 
      <li class="nav-item">
        <a class="nav-link" href="../view/configuracoes.php">
          <i class="fas fa-fw fa-cogs"></i>
          <span>Configurações</span></a>
      </li>-->
    </ul>
    
