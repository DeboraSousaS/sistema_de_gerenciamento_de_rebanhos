<?php

  session_start();

  if (@$_SESSION['id'] == '') {
      echo '<script>
              alert("Antes Disso Informe Seu E-mail e Senha!");
            window.location = "../view/index.php";
          </script>';
  }

  $numero = $_GET['numero'];
  include_once '../model/conexao.php';
  $id = $_SESSION['idfazenda'];
  $pdo = Database::connect();
  $sql = "SELECT * FROM usuarios, fazendas where usuarios.idfazenda = $id and usuarios.idfazenda=fazendas.id";
  //$sql = "SELECT * FROM fazendas where $id = id";
  $records = $pdo->prepare($sql);
  $records->execute();
  $result = $records->fetch(PDO::FETCH_ASSOC);

   $user = $_SESSION['id'];
    $pdo2 = Database::connect();
    $sql2 = "SELECT * FROM usuarios where $user = id";
    $records2 = $pdo2->prepare($sql2);
    $records2->execute();
    $result2 = $records2->fetch(PDO::FETCH_ASSOC);
  Database::disconnect();

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIGER</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
        include 'menu.php';
    ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-2x text-gray-200"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../view/perfil.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../model/sair.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-3">
            <h1 class="h4 mb-0 text-gray-900">Registrar Medida preventiva</h1>
          </div>

          <!-- Content Row -->
          
            <form action="../model/animal_med_preven.php" method="post">
              <div class="row">

                <div class="col-sm-3 form-group">
                  Número de identificação/Nome <input type="text" name="nume_nome" class="form-control" placeholder="Identificação" readonly value="<?php echo $numero?>">
                </div>

                 <div class="col-sm-2 mb-3 mb-sm-0">
                    Fazenda <input type="text" class="form-control form-control-user" placeholder="Nome da Fazenda" name="idfazenda" readonly value="<?php echo $result['nomefaz'];?>">
                    <input type = "hidden" name="idfazenda" value="<?php echo $result['idfazenda']; ?>">
                </div>

                <div class="col-sm-4 form-group">
                    Usuário <input type="text" name="iduser" class="form-control"placeholder="Nome completo" value="<?php echo $result2['nome']; ?>" readonly>
                    <input type = "hidden" name="iduser" value="<?php echo $result2['id']; ?>">
                </div>

                <div class="col-sm-3 form-group">
                  Método aplicado 
                  <select class="form-control" name="metodo">
                    <option>...</option>
                    <option>Limpeza de instalações</option>
                    <option>Rotação de pastos</option>
                    <option>Vermifugação</option>
                    <option>Outros</option>
                  </select>
                </div>

                <div class="col-sm-3 form-group">
                  Data de aplicação<input type="date" name="dataAplic" class="form-control" required>
                </div>

                <div class="col-sm-2 form-group">
                  Quantidade de doses <input type="text" name="quantDo" class="form-control" placeholder="Quantidade">
                </div>

                <div class="col-sm-7 form-group">
                  Observação(ões) <input type="text" name="obser" class="form-control" placeholder="Registre a(s) observação(ões)">
                </div>

                <div class="form-group col-md-12">
                  <input type="submit" name="buttoncadastrar" class=" btn btn-primary" value="Cadastrar">
                </div>

              </div>
            </form>


         


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © IFCE Campus Tauá 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php
    include '../view/logout.php';
  ?>

  <?php
    include 'imports.php';
  ?>

</body>

</html>
