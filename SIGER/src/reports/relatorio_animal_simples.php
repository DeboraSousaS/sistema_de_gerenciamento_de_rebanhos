<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIGER</title>

    <!-- Custom fonts for this template-->

    <!-- Custom styles for this template -->
    <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page onload="self.print();self.close();"-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #4b70dd;
            color: white;
            text-align: center;
            line-height: 1.5cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            background-color: #4b70dd;
            color: white;
            text-align: center;
            line-height: 1.5cm;
        }
    </style>
</head>
<body>
<!-- Define header and footer blocks before your content -->
<header>
    <h1 style="margin-top: 10px;">SIGER</h1>
    <p style="color:black">Relatório de Animais - Gerado em 12/12/2019</p>
</header>




<!-- Wrap the content of your PDF inside a main tag -->
<main style="margin-top: 50px">
    <table class="table table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th colspan="10" class="text-center text-primary">Relatório dos dados zootécnicos</th>
            </tr>

            <tr>
                <th colspan="3">Número de identificação</th>
                <td class="text-center" colspan="3">{{numeroIdent}}</td>
                <th colspan="3" colspan="3">Tipo de Identificação</th>
                <td class="text-center">{{tipoIdent}}</td>
            </tr>

            <tr>
                <th colspan="3">Data de Nascimento</th>
                <td class="text-center" colspan="3">{{datanasci}}</td>
                <th colspan="3">Peso ao nascimento</th>
                <td class="text-center" colspan="3">{{pesoInit}}</td>
            </tr>

            <tr>
                <th colspan="3">Pai</th>
                <td class="text-center" colspan="3">{{pai}}</td>
                <th colspan="3" colspan="3">Mãe</th>
                <td class="text-center" colspan="3">{{mae}}</td>
            </tr>

            <tr>
                <th colspan="3">Categoria</th>
                <td class="text-center"colspan="3">{{categoria}}</td>
                <th colspan="3">Origem</th>
                <td class="text-center" colspan="4">{{origem}}</td>
            </tr>

            <tr>
                <th colspan="3">Fazenda pertencente</th>
                <td class="text-center" colspan="3">{{fazenda}}</td>
                <th colspan="3">Nome da Fazenda</th>
                <td class="text-center" colspan="4">{{nomefaz}}</td>
            </tr>
        </thead>

    </table>
</main>

<footer>
    Copyright &copy; IFCE 2019
</footer>

</body>
</html>