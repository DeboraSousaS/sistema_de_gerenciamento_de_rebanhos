<?php
include_once 'conexao.php';
$pdo = Database::connect();
$id = $_GET['id'];

if ($id != null) {
    $sql = "SELECT * FROM usuarios WHERE id = :id";
    $records = $pdo->prepare($sql);
    $records->bindParam(':id', $id);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);
    if ($result != null) {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM usuarios WHERE id= ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($result['id']));
        echo
        '<script>
            alert("Pessoa excluído com sucesso!");
            window.location = "lista_usuarios.php";
        </script>';
    } else {
        echo
        '<script>
            alert("Você tentou excluir um Feirante inválido!");
            window.location = "../view/lista_usuarios.php";
        </script>';
    }
} else {
    echo
    '<script>
        alert("Você tentou apagar um Feirante inválido!");
        window.location = "../view/lista_usuarios.php";
    </script>';
}
Database::disconnect();
