<?php
include_once 'conexao.php';
$pdo = Database::connect();
$id = $_GET['id'];

if ($id != null) {
    $sql = "SELECT * FROM fazendas WHERE id = :id";
    $records = $pdo->prepare($sql);
    $records->bindParam(':id', $id);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);
    if ($result != null) {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM fazendas WHERE id= ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($result['id']));
        echo
        '<script>
            alert("Fazenda excluído com sucesso!");
            window.location = "lista_fazendas.php";
        </script>';
    } else {
        echo
        '<script>
            alert("Você tentou excluir uma fazenda inválido!");
            window.location = "../view/lista_fazendas.php";
        </script>';
    }
} else {
    echo
    '<script>
        alert("Você tentou apagar uma fazenda inválido!");
        window.location = "../view/lista_fazendas.php";
    </script>';
}
Database::disconnect();
