<?php
  session_start();
  $id = $_SESSION['idfazenda'];
  include_once 'conexao.php';
  $pdo = Database::connect();
  $sql = "SELECT * FROM usuarios where $id = idfazenda";
  $records = $pdo->prepare($sql);
  $records->execute();
  $result = $records->fetch(PDO::FETCH_ASSOC);

  $pdo2 = Database::connect();
  $sql2 = "SELECT * FROM usuarios, fazendas where usuarios.idfazenda = $id and usuarios.idfazenda=fazendas.id";
  $records2 = $pdo2->prepare($sql2);
  $records2->execute();
  $result2 = $records2->fetch(PDO::FETCH_ASSOC);
  Database::disconnect();
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIGER</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
        include '../view/menu.php';
    ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-2x text-gray-200"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../view/perfil.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>

       <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-3">
            <h1 class="h4 mb-0 text-gray-900">Visualizar fazenda</h1>
          </div>

          <!-- Content Row -->
          

             <form action="" method="post">
              <div class="row">

                <div class="col-sm-2 form-group">
                    Idenificação <input type="text" name="id" class="form-control"placeholder="Nome da fazenda" value="<?php echo $result['idfazenda']; ?>"readonly>
                </div>

                <div class="col-sm-3 form-group">
                    Nome <input type="text" name="nome" class="form-control"placeholder="Nome da fazenda" value="<?php echo $result2['nomefaz']; ?>"readonly>
                </div>

                <div class="col-sm-3 form-group">
                  Data de cadastro <input type="date" name="data" class="form-control" value="<?php echo $result2['data']; ?>"readonly>
                </div>

                  <div class="col-sm-2">
                    Senha <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Senha" name="senha" value="<?php echo $result2['senhaa']; ?>" readonly>
                  </div>
                  <div class="col-sm-2">
                    Confirmar senha<input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Confirmar senha" name="confsenha" value="<?php echo $result2['confsenhaa']; ?>" readonly>
                  </div>

                <!--<div class="form-group col-md-12">
                  <input type="submit" name="buttoncadastrar" class=" btn btn-primary" value="Cadastrar">
                </div>-->

              </div>
            </form>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © IFCE Campus Tauá 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php
    include '../view/logout.php';
  ?>

  <?php
    include '../view/imports.php';
  ?>

</body>

</html>