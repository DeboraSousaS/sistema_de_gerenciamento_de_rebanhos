<?php

    //cadastrar dados no banco de dados
    include_once 'conexao.php';//importar o arquivo de conecção do banco de dados
    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO desmame_animal(numeroIdent, idfazenda, iduser, dataDes, peso, idade, obs) values(?,?,?,?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['numeroIdent'], $data['idfazenda'], $data['iduser'], $data['dataDes'],$data['peso'],$data['idade'],$data['obs']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../model/lista_animais_desmame.php";
        </script>'; 
}

?>