<?php

    //cadastrar dados no banco de dados
    include_once 'conexao.php';//importar o arquivo de conecção do banco de dados

    function animalexistente($numero){
        $pdo = Database::connect();
        $sql = "SELECT * FROM animais where numeroIdent = $numero";
        $records = $pdo->prepare($sql);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if($result['id']==null){
            return true;
        }else{
            return false;
        }
    }


    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        if(animalexistente($data['numeroIdent'])){
            $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO animais(pai, mae, datanasci, numeroIdent, tipoIdent, categoria, origem, pesoInit, fazenda, user, obs) values(?,?,?,?,?,?,?,?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['pai'], $data['mae'],$data['datanasci'], $data['numeroIdent'],$data['tipoIdent'],$data['categoria'],$data['origem'],$data['pesoInit'],$data['fazenda'],$data['user'],$data['obs']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../view/add_animal.php";
        </script>'; 
        }else{
            echo
        '<script>
            alert("Numero Animal Já Existe!!");
            window.location = history.go(-1);
        </script>'; 
        }

    
        
}

?>