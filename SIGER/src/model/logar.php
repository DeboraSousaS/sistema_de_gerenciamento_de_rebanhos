<?php
session_start(); 
echo '<meta charset="utf-8">';
require( 'conexao.php' );
$pdo = Database::connect();
$data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
//var_dump($data);
$email = $data['email'];
$senha = $data['senha'];

$sql = "SELECT * FROM usuarios WHERE email = :email";
$records = $pdo->prepare($sql);
$records->bindParam(':email', $email);
$records->execute();

$result = $records->fetch(PDO::FETCH_ASSOC);

if (count($result) > 0 && ($senha == $result['senha'])) {
    
	$_SESSION['usuarios'] = $result['nome'];//Usuário
    $_SESSION['id'] = $result['id'];
    $_SESSION['idfazenda'] = $result['idfazenda'];
    //$_SESSION['acesso'] = $result['permissao'];
    if ($_SESSION['id'] == $result['id']){
        echo '<script>
             window.location = "../view/home.php";
        </script>';
    }
    
} else {
    echo '<script>
            alert("Dados incorretos!!");
            window.location = "../view/index.php";
        </script>';
}
Database::disconnect();


