<?php

//cadastrar dados no banco de dados
    include_once 'conexao.php';//importar o arquivo de conecção do banco de dados

    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO fazendas(nomefaz, data, senhaa, confsenhaa) values(?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['nomefaz'], $data['data'], $data['senhaa'], $data['confsenhaa']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../view/entrar_fazenda.php";
        </script>'; 
}
?>
