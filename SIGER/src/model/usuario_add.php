<?php

    //cadastrar dados no banco de dados
    include_once 'conexao.php';//importar o arquivo de conecção do banco de dados
    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO usuarios(nome, datanasci, sexo, endereco, numero, bairro, cep, cidade, estado, telefone, email, senha, confsenha) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['nome'], $data['datanasci'],$data['sexo'], $data['endereco'],$data['numero'],$data['bairro'],$data['cep'],$data['cidade'],$data['estado'],$data['telefone'],$data['email'],$data['senha'],$data['confsenha']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../view/index.php";
        </script>'; 
}

?>