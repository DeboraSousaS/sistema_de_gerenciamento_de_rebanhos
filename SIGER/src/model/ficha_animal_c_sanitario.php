<?php
   session_start();
  if (@$_SESSION['id'] == '') {
      echo '<script>
              alert("Antes Disso Informe Seu E-mail e Senha!");
              window.location = "index.php";
          </script>';
  }
    $numero = $_GET['numero'];
    $id = $_SESSION['idfazenda'];
    $user = $_SESSION['id'];
    include_once 'conexao.php';
    $pdo = Database::connect();
    $sql = "SELECT * FROM doenca_animal, fazendas where doenca_animal.idfazenda = fazendas.id and doenca_animal.numeroIdent = $numero ORDER BY dataInterv";
    $records = $pdo->prepare($sql);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);

    $user = $_SESSION['id'];
    $pdo2 = Database::connect();
    $sql2 = "SELECT * FROM usuarios where $user = id";
    $records2 = $pdo2->prepare($sql2);
    $records2->execute();
    $result2 = $records2->fetch(PDO::FETCH_ASSOC);

    $pdo3 = Database::connect();
    $sql3 = "SELECT * FROM med_prev_animal, fazendas where med_prev_animal.idfazenda = fazendas.id and med_prev_animal.nume_nome = $numero ORDER BY dataAplic";
    $records3 = $pdo3->prepare($sql3);
    $records3->execute();
    $result3 = $records3->fetch(PDO::FETCH_ASSOC);

    $pdo4 = Database::connect();
    $sql4 = "SELECT * FROM vacina_animal, fazendas where vacina_animal.idfazenda = fazendas.id and vacina_animal.numeroIdent = $numero ORDER BY dataApli";
    $records4 = $pdo4->prepare($sql4);
    $records4->execute();
    $result4 = $records4->fetch(PDO::FETCH_ASSOC);

    Database::disconnect();
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIGER</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        @media print {
            .navbar-nav, button {
                display:none !important;
            }
            .container-fluid{
                margin-top: 50px;
            }
            .title-1{
                text-align: center;
                font-size: 25px;
            }
            .title-2{
                text-align: center;
                font-size: 18px;
            }

        }
    </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
        include '../view/menu.php';
    ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-2x text-gray-200"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../view/perfil.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../model/sair.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <div class="container-fluid">

          <!-- Page Heading 
          <h1 class="h4 mb-2 text-gray-900">Lista de animais</h1>-->
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary title-1">Ficha do Animal Controle Sanitário</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                  
                  <tr>
                    <th colspan="12" class="text-center text-primary title-2">Relatório Zootécnico Sanitário</th>
                  </tr>

                  <tr>
                    <th colspan="12" class="text-center">Doença(s)</th>
                  </tr>

                  
                  <tr>
                    <th colspan="2">Número de identificação</th>
                    <td class="text-center" colspan="2"><?php echo $numero;?></td>
                    <th colspan="2">Fazenda</th>
                    <td class="text-center" colspan="2"><?php echo $result['nomefaz'];?></td>
                    <th colspan="2">Usuário</th>
                    <td class="text-center" colspan="2"><?php echo $result2['nome'];?></td>
                  </tr>

                    <?php foreach ($pdo->query($sql) as $row) {?>
                  
                  <tr>
                    <th colspan="2">Data de Intervenção</th>
                    <td class="text-center" colspan="2"><?php echo $row['dataInterv'];?></td>                    
                    <th colspan="2">Doença</th>
                    <td class="text-center" colspan="2"><?php echo $row['doenca'];?></td>
                    <th colspan="2" colspan="3">Tratamento</th>
                    <td class="text-center" colspan="2"><?php echo $row['tratamento'];?></td>
                  </tr>

                  <tr>
                    <th colspan="2">Quantidades de doses</th>
                    <td class="text-center"colspan="3"><?php echo $row['quantDos'];?></td>
                    <th colspan="3">Observações</th>
                    <td class="text-center" colspan="4"><?php echo $row['obss'];?></td>
                  </tr>
                  <?php
                    }
                    ?>

                  <tr>
                    <th colspan="12" class="text-center">Medida(s) preventiva(s)</th>
                  </tr>

                  <?php foreach ($pdo3->query($sql3) as $ro) {?>
                  
                  <tr>
                    <th colspan="3">Metodo aplicado</th>
                    <td class="text-center" colspan="3"><?php echo $ro['metodo'];?></td>                    
                    <th colspan="3">Data aplicação</th>
                    <td class="text-center" colspan="3"><?php echo $ro['dataAplic'];?></td>
                  </tr>

                  <tr>
                    <th colspan="3">Quantidade de doses</th>
                    <td class="text-center" colspan="3"><?php echo $ro['quantDo'];?></td>
                    <th colspan="3">Observações</th>
                    <td class="text-center" colspan="3"><?php echo $ro['obser'];?></td>
                  </tr>
                  <?php
                    }
                    ?>


                    <tr>
                    <th colspan="12" class="text-center">Vacinação</th>
                  </tr>

                  <?php foreach ($pdo4->query($sql4) as $ro) {?>
                  
                  <tr>                   
                    <th colspan="3">Data aplicação</th>
                    <td class="text-center" colspan="3"><?php echo $ro['dataApli'];?></td>
                    <th colspan="3">Vacina</th>
                    <td class="text-center" colspan="3"><?php echo $ro['vacina'];?></td> 
                  </tr>

                  <tr>
                    <th colspan="6">Observações</th>
                    <td class="text-center" colspan="6"><?php echo $ro['oobs'];?></td>
                  </tr>
                  <?php
                    }
                    ?>
                </thead>

              </table>

              <form name="" method="get" action=""> 
                  <input type="hidden" name="numero" value="<?php echo $numero;?>">
                  <!--<button type="submit" value="imprimir" <a class="btn btn-info">
                      <i class="fas fa-download fa-sm text-white-50"action="ficha_animal_simples_imprimir.php"></i> Gerar PDF</a>
                  </button>-->

                  <button type="submit" value="imprimir" <a class="btn btn-info" onclick="window.print();">
                      <i class="fas fa-download fa-sm text-white-50"></i> Imprimir</a>
                  </button>
              </form>
              </div>
            </div>
          </div>

        </div>

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © IFCE Campus Tauá 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php
    include '../view/logout.php';
  ?>

  <?php
    include '../view/imports.php';
  ?>

</body>

</html>
