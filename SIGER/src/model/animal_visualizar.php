<?php

include_once 'conexao.php';
$id = $_GET['id'];
$pdo = Database::connect();
$sql = "SELECT * FROM animais where id = $id";
$records = $pdo->prepare($sql);
$records->execute();
$result = $records->fetch(PDO::FETCH_ASSOC);
Database::disconnect();

/*$pdo2 = Database::connect();
$sql2 = "SELECT * FROM animais, saida_animal where animais.id = $id and animais.numeroIdent=saida_animal.numeroIdent";
$records2 = $pdo2->prepare($sql2);
$records2->execute();
$result2 = $records2->fetch(PDO::FETCH_ASSOC);
Database::disconnect();
CASO PRECISE JUNTAR DUAS TABELAS E APRESENTAR OS DADOS*/

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIGER</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
        include '../view/menu.php';
    ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-2x text-gray-200"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../view/perfil.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../model/sair.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-3">
            <h1 class="h4 mb-0 text-gray-900">Visualizar animal</h1>
          </div>
          <form action="" method="post">
              <div class="row">

                <div class="col-sm-3 form-group">
                    Pai <input type="text" name="pai" class="form-control"placeholder="Número da mãe" value="<?php echo $result['pai']; ?>" readonly>
                </div>

                <div class="col-sm-3 form-group">
                    Mãe <input type="text" name="mae" class="form-control"placeholder="Número do pai" value="<?php echo $result['mae']; ?>" readonly>
                </div>

                <div class="col-sm-3 form-group">
                  Data de Nascimento <input type="date" name="datanasci" class="form-control" value="<?php echo $result['datanasci']; ?>" readonly>
                </div>

                <div class="col-sm-3 form-group">
                    Número de identificação <input type="text" name="numeroIdent" class="form-control"placeholder="Numero de identificação" value="<?php echo $result['numeroIdent']; ?>" readonly>
                </div>

                <div class="col-sm-3 form-group">
                  Tipo de Identificação 
                  <select class="form-control" name="tipoIdent"value="<?php echo $result['tipoIdent']; ?>" readonly>
                    <option>Microchip</option>
                    <option>Placa Metálica</option>
                    <option>Placa de Plástico</option>
                    <option>Coleira</option>
                    <option>Tatuagem</option>
                    <option>Brinco Eletrônico</option>
                    <option>Brinco Plástico</option>
                  </select>
                </div>

                <div class="col-sm-3 form-group">
                  Categoria
                  <select class="form-control" name="categoria" value="<?php echo $result['categoria']; ?>" readonly>
                    <option>Novilha</option>
                    <option>Boi</option>
                    <option>Garrote</option>
                    <option>Cabra</option>
                    <option>Cabrita</option>
                    <option>Cabritinha</option>
                    <option>Bode</option>
                    <option>Cabrito</option>
                    <option>Cabritinho</option>
                    <option>Capão</option>
                    <option>Borrego</option>
                    <option>Cordeiro</option>
                    <option>Carneiro</option>
                    <option>Cordeira</option>
                    <option>Ovelha</option>
                    <option>Rufião</option>
                    <option>Matriz</option>
                    <option>Reprodutor</option>
                    <option>Bezerro</option>
                    <option>Bezerra</option>
                  </select>
                </div>

                <div class="col-sm-3 form-group">
                    Origem 
                  <select class="form-control" name="origem" value="<?php echo $result['origem']; ?>" readonly>
                    <option>Animal do rebanho</option>
                    <option>Animal comprado/doado</option>
                  </select>
                </div>

                <div class="col-sm-3 form-group">
                  Peso ao nascimento <input type="text" name="pesoInit" class="form-control" placeholder="Peso ao nascimento" value="<?php echo $result['pesoInit']; ?>" readonly>
                </div>

                <div class="col-sm-3 form-group">
                  Fazenda pertencente <input type="text" name="fazenda" class="form-control" placeholder="Fazenda" value="<?php echo $result['fazenda']; ?>" readonly>
                </div>


                <div class="col-sm-9 form-group">
                  Observação(ões) <input type="text" name="obs" class="form-control" placeholder="Registre a(s) observação(ões)" value="<?php echo $result['obs']; ?>" readonly>
                </div>

                
              </div>
            </form>
            
              <div class="row">
                <div class="form-group col-md-4">
                    <br>
                    <form name="editar" method="get" action="animal_editar.php" >
                    <input type="hidden" name="id" value="<?php echo $result['id'];?>">  
                    <button type="submit" value="editar" class="btn btn-primary">Atualizar</button>
                    </form> 
                  </div>
                </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © IFCE Campus Tauá 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php
    include '../view/logout.php';
  ?>

  <?php
    include '../view/imports.php';
  ?>

</body>

</html>