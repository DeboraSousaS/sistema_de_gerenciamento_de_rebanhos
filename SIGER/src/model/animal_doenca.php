<?php

    //cadastrar dados no banco de dados
    include_once 'conexao.php';//importar o arquivo de conecção do banco de dados
    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO doenca_animal(numeroIdent, idfazenda, iduser, dataInterv, doenca, tratamento, quantDos, obss) values(?,?,?,?,?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['numeroIdent'], $data['idfazenda'], $data['iduser'], $data['dataInterv'],$data['doenca'], $data['tratamento'], $data['quantDos'], $data['obss']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../model/lista_animais_doenca.php";
        </script>'; 
}

?>