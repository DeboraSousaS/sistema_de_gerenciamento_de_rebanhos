<?php
  session_start();
  if (@$_SESSION['id'] == '') {
      echo '<script>
              alert("Antes Disso Informe Seu E-mail e Senha!");
              window.location = "index.php";
          </script>';
  }
  
  $id = $_SESSION['idfazenda'];
    include_once 'conexao.php';
    $pdo = Database::connect();
    $sql = "SELECT * FROM usuarios where $id = idfazenda";
    $records = $pdo->prepare($sql);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);

    $pdo2 = Database::connect();
    $sql2 = "SELECT * FROM animais where animais.fazenda = $id and animais.numeroIdent NOT IN (SELECT saida_animal.numeroIdent FROM saida_animal) AND (animais.categoria LIKE '%Novilha%' OR animais.categoria LIKE '%Cabra%' OR animais.categoria LIKE '%Cabritinha%' OR animais.categoria LIKE '%Cordeira%' OR animais.categoria LIKE '%Ovelha%' OR animais.categoria LIKE '%Matriz%' OR animais.categoria LIKE '%Bezerra%')";
    $records2 = $pdo2->prepare($sql2);
    $records2->execute();
    $result2 = $records2->fetch(PDO::FETCH_ASSOC);
    Database::disconnect();
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SIGER</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php
        include '../view/menu.php';
    ?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-2x text-gray-200"></i>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../view/perfil.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../model/sair.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Lista de animais</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr class="text-center">
                    <th>Número de identificação</th>
              			<th>Categoria</th>
              			<th>Confimação de prenhez</th>
                  </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($pdo2->query($sql2) as $row) {?>
                    <tr>
                    <td class="text-center"><?php echo $row['numeroIdent'];?></td>

                    <td class="text-center"><?php echo $row['categoria'];?></td>
                                                    
                    <td class="td-actions text-center">
                      <form name="codigo" method="get" action="../view/conf_prenhez_animal.php" > <input type="hidden" name="numero" value="<?php echo $row['numeroIdent'];?>">  <button type="submit" value="" <a class="btn btn-info btn-circle"><i class="fas fa-check">  </i></a></button></form>
                    </td>

                    
                  </tr>
                   <?php
                    }
                    ?>    
                </tbody>

                
              </table>
              </div>
            </div>
          </div>

        </div>

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © IFCE Campus Tauá 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php
    include '../view/logout.php';
  ?>

  <?php
    include '../view/imports.php';
  ?>

</body>

</html>
