<?php
//SELECT LAST_INSERT_ID; // saber o último ID inserido automaticamente mysql_insert_id();
    include_once 'conexao.php';
    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($data['buttoncadastrar'] == 'Cadastrar'){
    
        $pdo = Database::connect();//fazer a conecção
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO usuarios(idfazenda, nome, email, senha, confsenha) values(?,?,?,?,?)";
        $q = $pdo->prepare($sql);//execultar o sql que foi criado
        $q->execute(array($data['idfazenda'], $data['nome'],$data['email'],$data['senha'],$data['confsenha']));
        Database::disconnect();
        echo
        '<script>
            alert("Dados cadastrados com sucesso! ");
            window.location = "../model/lista_usuarios.php";
        </script>';
}
?>
