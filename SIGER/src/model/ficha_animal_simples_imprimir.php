<?php
    include '../view/imports.php';
    include_once 'conexao.php';
    require_once '../../vendor/dompdf/lib/html5lib/Parser.php';
    require_once '../../vendor/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
    require_once '../../vendor/dompdf/lib/php-font-lib/src/FontLib/Font.php';

    require_once '../../vendor/dompdf/lib/php-svg-lib/src/autoload.php';
    require_once '../../vendor/dompdf/src/Autoloader.php';
    require_once '../../vendor/dompdf/src/FontMetrics.php';

    use Dompdf\Dompdf;
    use Dompdf\Options;

    $options = new Options();
    $options->set('isRemoteEnabled', TRUE);
    // instantiate and use the dompdf class
    $dompdf = new Dompdf($options);

    //HTML
    $numero = $_GET['numero'];
    $pdo = Database::connect();
    $stmt = $pdo->query("SELECT * FROM animais, fazendas  where animais.numeroIdent = $numero and fazendas.id = animais.fazenda");
    $animal = $stmt->fetch();

    Database::disconnect();

    $html = file_get_contents('../reports/relatorio_animal_simples.php');

    $html = str_replace("{{numeroIdent}}", $animal["numeroIdent"], $html);
    $html = str_replace("{{tipoIdent}}", $animal["tipoIdent"], $html);
    $html = str_replace("{{datanasci}}", $animal["datanasci"], $html);
    $html = str_replace("{{pesoInit}}", $animal["pesoInit"], $html);
    $html = str_replace("{{pai}}", $animal["pai"], $html);
    $html = str_replace("{{mae}}", $animal["mae"], $html);
    $html = str_replace("{{categoria}}", $animal["categoria"], $html);
    $html = str_replace("{{origem}}", $animal["origem"], $html);
    $html = str_replace("{{fazenda}}", $animal["fazenda"], $html);
    $html = str_replace("{{nomefaz}}", $animal["nomefaz"], $html);

    // carregamos o código HTML no nosso arquivo PDF
    $dompdf->loadHtml($html,'UTF-8');

    // (Opcional) Defina o tamanho (A4, A3, A2, etc) e a oritenação do papel, que pode ser 'portrait' (em pé) ou 'landscape' (deitado)
    $dompdf->setPaper('A4', 'landscape');

    // Renderizar o documento
    $dompdf->render();

    // pega o código fonte do novo arquivo PDF gerado
    $output = $dompdf->output();

    // defina aqui o nome do arquivo que você quer que seja salvo
    file_put_contents("../files/gerador.pdf", $output);
    // redirecionamos o usuário para o download do arquivo
    die("<script>location.href='../files/gerador.pdf';</script>");
?>

