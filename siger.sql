-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 14-Jul-2019 às 16:30
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siger`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `animais`
--

DROP TABLE IF EXISTS `animais`;
CREATE TABLE IF NOT EXISTS `animais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai` varchar(200) NOT NULL,
  `mae` varchar(200) NOT NULL,
  `datanasci` varchar(200) NOT NULL,
  `numeroIdent` varchar(200) NOT NULL,
  `tipoIdent` varchar(200) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `origem` varchar(200) NOT NULL,
  `pesoInit` varchar(200) NOT NULL,
  `fazenda` varchar(500) NOT NULL,
  `user` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `animais`
--

INSERT INTO `animais` (`id`, `pai`, `mae`, `datanasci`, `numeroIdent`, `tipoIdent`, `categoria`, `origem`, `pesoInit`, `fazenda`, `user`, `obs`) VALUES
(1, '788', '78', '2019-05-31', '7788', 'Microchip', 'Novilha', 'Animal do rebanho', '6', '2', '', ''),
(2, '67', '7', '2019-05-22', '78', 'Microchip', 'Novilha', 'Animal do rebanho', '6 kg', '2', '', 'Teste'),
(3, '4', '5', '2019-05-22', '90', 'Microchip', 'Novilha', 'Animal do rebanho', '5', '5', '', 'Teste'),
(10, 'ess', 'tes', '2019-05-31', '9', 'Microchip', 'Novilha', 'Animal do rebanho', '7', '5', '', 'Tester'),
(6, 'aaa', 'bbb', '2019-05-27', '900', 'Microchip', 'Novilha', 'Animal do rebanho', '', '2', '', ''),
(8, 'bb', 'bcc', '2019-05-30', '99', 'Microchip', 'Novilha', 'Animal do rebanho', '6', '5', '', 'Teste'),
(9, 'rrr', 'ttt', '2019-05-30', '5', 'Microchip', 'Novilha', 'Animal do rebanho', '5', '8', '', 'Teste'),
(11, 'te', 'se', '2019-06-05', '88', 'Microchip', 'Novilha', 'Animal comprado/doado', '3', '8', '', 'testte'),
(13, 't2', 'te2', '2019-06-05', '7', 'Microchip', 'Novilha', 'Animal do rebanho', '4', '11', '', ''),
(14, '11', '22', '2019-06-05', '45', 'Microchip', 'Novilha', 'Animal do rebanho', '3', '12', '', 'teste final'),
(15, 'testes', 'tte', '2019-07-10', '100', 'Brinco Plástico', 'Ovelha', 'Animal do rebanho', '5', '2', '', 'teste de listagem de saida'),
(16, '', '', '2019-07-10', '098', '...', '...', '...', '', '5', '14', ''),
(17, '', '', '2019-07-10', '333', '...', '...', '...', '', '5', '14', ''),
(18, '', '', '2019-07-10', '456', '...', '...', '...', '', '2', '2', 'teste de campos'),
(19, '', '', '2019-07-12', '65', 'Placa Metálica', 'Cabra', '...', '', '2', '2', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cobert_animal`
--

DROP TABLE IF EXISTS `cobert_animal`;
CREATE TABLE IF NOT EXISTS `cobert_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataCober` varchar(200) NOT NULL,
  `reprodutor` varchar(200) NOT NULL,
  `tipoCob` varchar(200) NOT NULL,
  `quant` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cobert_animal`
--

INSERT INTO `cobert_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataCober`, `reprodutor`, `tipoCob`, `quant`, `obs`) VALUES
(1, '00029', '', '', '2019-05-17', '005', 'Monta natural', '2', ''),
(2, '', '', '', '', '', 'Monta natural', '', ''),
(3, '', '', '', '', '', 'Monta natural', '', ''),
(6, '9', '5', '14', '2019-07-08', '54', 'Monta natural', '2', ''),
(5, '098', '5', '14', '2019-07-10', '', 'Monta natural', '', ''),
(7, '9', '5', '14', '2019-07-17', '54', 'Inseminação artificial', '5', 'informa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `desmame_animal`
--

DROP TABLE IF EXISTS `desmame_animal`;
CREATE TABLE IF NOT EXISTS `desmame_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataDes` varchar(200) NOT NULL,
  `peso` varchar(200) NOT NULL,
  `idade` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `desmame_animal`
--

INSERT INTO `desmame_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataDes`, `peso`, `idade`, `obs`) VALUES
(1, '00029', '', '', '2019-05-23', '30', '4 meses', ''),
(2, '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', ''),
(4, '098', '5', '14', '2019-07-10', '', '', ''),
(5, '9', '5', '14', '2019-07-31', '50', '4 meses', 'Teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `doenca_animal`
--

DROP TABLE IF EXISTS `doenca_animal`;
CREATE TABLE IF NOT EXISTS `doenca_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` int(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataInterv` varchar(200) NOT NULL,
  `doenca` varchar(200) NOT NULL,
  `tratamento` varchar(200) NOT NULL,
  `quantDos` varchar(200) NOT NULL,
  `obss` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `doenca_animal`
--

INSERT INTO `doenca_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataInterv`, `doenca`, `tratamento`, `quantDos`, `obss`) VALUES
(1, '00029', 0, '', '2019-05-17', 'Artrite da articulação', 'Não identificado', '', '000'),
(2, '78', 2, '', '2019-05-22', 'Artrite da articulação', 'Não identificado', '0', 'iii'),
(3, '', 0, '', '', '', '', '', ''),
(5, '9', 5, '14', '2019-05-31', 'tttt', 'fff', '3', 'te'),
(6, '78', 2, '', '2019-07-10', 'Artrite da articulação', 'Não identificado', '1', 'Teste 2'),
(7, '456', 2, '2', '2019-07-10', '', '', '', ''),
(8, '90', 5, '14', '2019-07-11', 'Artrite da articulação', 'Não identificado', '1', 'Teste'),
(9, '90', 5, '14', '2019-07-12', 'ttt', 'ttt', '1', 'trtr'),
(10, '100', 2, '2', '2019-07-12', 'Artrite da articulação', 'Não identificado', '2', 'Teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enc_lact_animal`
--

DROP TABLE IF EXISTS `enc_lact_animal`;
CREATE TABLE IF NOT EXISTS `enc_lact_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataEncer` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `enc_lact_animal`
--

INSERT INTO `enc_lact_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataEncer`, `obs`) VALUES
(1, '00029', '', '', '2019-05-17', ''),
(2, '', '', '', '', ''),
(3, '333', '5', '14', '2019-07-10', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fazendas`
--

DROP TABLE IF EXISTS `fazendas`;
CREATE TABLE IF NOT EXISTS `fazendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomefaz` varchar(500) NOT NULL,
  `data` varchar(500) NOT NULL,
  `senhaa` varchar(500) NOT NULL,
  `confsenhaa` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fazendas`
--

INSERT INTO `fazendas` (`id`, `nomefaz`, `data`, `senhaa`, `confsenhaa`) VALUES
(1, 'Riacho verde', '2019-05-28', '12', '12'),
(2, 'Santo Antonio', '2019-05-28', '123', '123'),
(3, 'Debora', '2019-05-28', '12', '12'),
(11, 'Santa Tereza', '2019-06-05', '1234', '1234'),
(5, 'SEDETE', '2019-05-29', 'sedete', 'sedete'),
(6, 'Massapé', '2019-05-30', '123', '123'),
(7, 'ST', '2019-05-30', '123', '123'),
(8, 'SJ', '2019-05-30', '21', '21'),
(12, 'Morada Nova', '2019-06-05', '12', '12'),
(13, 'Santo Antonio 1', '2019-07-10', '12', '12'),
(14, 'Santo Antonio 1', '2019-07-10', '12', '12'),
(15, 'SJyy', '2019-07-10', '12', '12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `leite_animal`
--

DROP TABLE IF EXISTS `leite_animal`;
CREATE TABLE IF NOT EXISTS `leite_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `iniesgot` varchar(200) NOT NULL,
  `fimesgot` varchar(200) NOT NULL,
  `dataOrd` varchar(200) NOT NULL,
  `quantOrd` varchar(200) NOT NULL,
  `quantLitro` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `leite_animal`
--

INSERT INTO `leite_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `iniesgot`, `fimesgot`, `dataOrd`, `quantOrd`, `quantLitro`, `obs`) VALUES
(1, '00029', '', '', '2019-05-05', '2019-05-12', '2019-05-17', 'Uma', '5 litros', ''),
(2, '', '', '', '', '', '', 'Uma', '', ''),
(3, '333', '5', '14', '2019-07-01', '2019-07-08', '2019-07-10', 'Duas', '10', ''),
(5, '78', '2', '2', '2019-07-03', '2019-07-10', '2019-07-11', 'Duas', '9', 'Teste'),
(6, '78', '2', '2', '', '', '2019-07-13', 'Duas', '10', ''),
(7, '90', '5', '14', '2019-07-01', '2019-07-08', '2019-07-09', 'Duas', '5', 'Teste'),
(8, '90', '5', '14', '', '', '2019-07-10', 'Duas', '10', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `med_prev_animal`
--

DROP TABLE IF EXISTS `med_prev_animal`;
CREATE TABLE IF NOT EXISTS `med_prev_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume_nome` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `metodo` varchar(200) NOT NULL,
  `dataAplic` varchar(300) NOT NULL,
  `quantDo` varchar(300) NOT NULL,
  `obser` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `med_prev_animal`
--

INSERT INTO `med_prev_animal` (`id`, `nume_nome`, `idfazenda`, `iduser`, `metodo`, `dataAplic`, `quantDo`, `obser`) VALUES
(1, '00029', '', '', '2019-05-17', 'Altec', '2', ''),
(2, '78', '2', '', 'h', '2019-05-22', '1', 'Teste'),
(3, '', '', '', 'Limpeza de instalações', '', '', ''),
(4, '9', '', '', 'Vermifugação', '2019-05-31', '7', 'tese'),
(5, '99', '', '', 'Limpeza de instalações', '2019-05-31', '2', 'teste1'),
(6, '456', '2', '2', 'Rotação de pastos', '2019-07-10', '3', 'ttt'),
(7, '90', '5', '14', 'Vermifugação', '2019-07-11', '2', ''),
(8, '90', '5', '14', 'Rotação de pastos', '2019-07-11', '1', ''),
(9, '78', '2', '2', 'Vermifugação', '2019-07-12', '5', 'apresentar dados');

-- --------------------------------------------------------

--
-- Estrutura da tabela `partos_animal`
--

DROP TABLE IF EXISTS `partos_animal`;
CREATE TABLE IF NOT EXISTS `partos_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataPart` varchar(200) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `partos_animal`
--

INSERT INTO `partos_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataPart`, `tipo`, `obs`) VALUES
(1, '00029', '', '', '2019-07-12', 'Simples', ''),
(2, '00029', '', '', '2019-05-31', 'Simples', ''),
(3, '', '', '', '', 'Simples', ''),
(4, '', '', '', '', 'Simples', ''),
(5, '098', '5', '14', '2019-07-10', 'Simples', ''),
(7, '9', '5', '14', '2020-09-20', 'Simples', 'Teste 4');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pesagem_animal`
--

DROP TABLE IF EXISTS `pesagem_animal`;
CREATE TABLE IF NOT EXISTS `pesagem_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataPesa` varchar(200) NOT NULL,
  `peeso` varchar(200) NOT NULL,
  `idadee` varchar(200) NOT NULL,
  `obbs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pesagem_animal`
--

INSERT INTO `pesagem_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataPesa`, `peeso`, `idadee`, `obbs`) VALUES
(1, '00029', '', '', '2019-05-17', '50 kg', '5 meses', ''),
(2, '78', '', '', '2019-05-23', '50 kg', '11 meses', '1 pesagem'),
(3, '78', '', '', '2019-04-19', '40 kg', '10.9', '2 pesagem'),
(4, '333', '5', '14', '2019-07-04', '60', '7', ''),
(5, '333', '5', '14', '2019-07-10', '62', '7.3', ''),
(6, '333', '5', '14', '2019-05-10', '50', '', ''),
(7, '333', '5', '14', '2019-07-11', '70', '', ''),
(8, '90', '5', '14', '2019-07-13', '50 kg', '5 meses', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `prenhez_animal`
--

DROP TABLE IF EXISTS `prenhez_animal`;
CREATE TABLE IF NOT EXISTS `prenhez_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataCon` varchar(200) NOT NULL,
  `dataParto` varchar(200) NOT NULL,
  `obs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `prenhez_animal`
--

INSERT INTO `prenhez_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataCon`, `dataParto`, `obs`) VALUES
(1, '00029', '', '', '2019-05-17', '2019-08-09', ''),
(2, '78', '', '', '2019-05-22', '2019-08-21', 'Teste s'),
(3, '78', '', '', '2019-05-22', '2019-08-21', 'Teste s'),
(6, '9', '5', '14', '2019-07-12', '2020-04-17', 'Teste'),
(5, '098', '5', '14', '2019-07-10', '2019-12-26', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `saida_animal`
--

DROP TABLE IF EXISTS `saida_animal`;
CREATE TABLE IF NOT EXISTS `saida_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(300) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `datasaida` varchar(300) NOT NULL,
  `motivo` varchar(300) NOT NULL,
  `causa` varchar(300) NOT NULL,
  `obbs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `saida_animal`
--

INSERT INTO `saida_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `datasaida`, `motivo`, `causa`, `obbs`) VALUES
(15, '100', '2', '0', '2019-07-10', 'Doação', 'Reprodudor/Matriz', ''),
(18, '9', '5', '14', '2019-07-10', '...', '...', ''),
(5, '99', '5', '0', '2019-07-10', 'Doação', 'Sem Informação', 'Teste de arquivar'),
(14, '900', '2', '0', '2019-07-10', 'Furto', 'Furto', 'Teste de listagem'),
(19, '78', '2', '2', '2019-07-10', '...', '...', 'nome fazenda'),
(20, '333', '5', '', '2019-07-10', '...', '...', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idfazenda` varchar(800) NOT NULL,
  `nome` varchar(500) NOT NULL,
  `email` varchar(300) NOT NULL,
  `senha` varchar(300) NOT NULL,
  `confsenha` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `idfazenda`, `nome`, `email`, `senha`, `confsenha`) VALUES
(2, '2', 'WISTON GOMES MOREIRA DA SILVA', 'wistongomes@gmail.com', '12', '12'),
(11, '10', 'Valeria sousa', 'valeria@gmail.com', '1234', '1234'),
(13, '12', 'Cirene Mendes', 'cirenemendes@gmail.com', '12', '12'),
(5, '5', 'Cleiton sedete', 'cleitoncdsb@hotmail.com', 'cdsb', 'cdsb'),
(7, '6', 'Helena Maria da Silva Sousa', 'helenasilva@gmail.com', '1234', '1234'),
(8, '7', 'Raimundo Miguel', 'raimundo@gmail.com', '11', '11'),
(9, '8', 'Maria da silva', 'maria@gmail.com', '123', '123'),
(14, '5', 'Felipe Feitosa Almeida', 'felipealmeida@gmail.com', '123', '123'),
(16, '2', 'teste de cammpo', 'testedecampo@gmail.com', '123', '123'),
(17, '14', 'helena mendes da silva', 'helenamedes@gmail.com', '12', '12'),
(18, '15', 'Débora da Silva Sousa', 'deborasousataua@gmail.com', '123', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vacina_animal`
--

DROP TABLE IF EXISTS `vacina_animal`;
CREATE TABLE IF NOT EXISTS `vacina_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdent` varchar(200) NOT NULL,
  `idfazenda` varchar(200) NOT NULL,
  `iduser` varchar(200) NOT NULL,
  `dataApli` varchar(200) NOT NULL,
  `vacina` varchar(300) NOT NULL,
  `oobs` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vacina_animal`
--

INSERT INTO `vacina_animal` (`id`, `numeroIdent`, `idfazenda`, `iduser`, `dataApli`, `vacina`, `oobs`) VALUES
(1, '00029', '', '', '2019-05-17', 'Botulinomax', ''),
(2, '7788', '', '', '2019-05-22', 'Botulinomax', 'Teste'),
(3, '78', '2', '', '2019-05-22', 'Botulinomax', 'Teste 4'),
(4, '', '', '', '', '', ''),
(5, '90', '5', '14', '2019-07-10', '', ''),
(6, '78', '2', '2', '2019-07-12', 'Botulinomax', 'Teste de dados'),
(7, '100', '2', '2', '2019-07-12', 'Botulinomax', 'Teste de dados');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
